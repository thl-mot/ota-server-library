#include <ESP8266WiFi.h>
#include <OtaServerConnector.h>

const char* ssid = "";
const char* password = "";

#define MY_VERSION "1.1"
#define MY_APPLICATION_ID ""

OtaServerConnector ota("my-firmware.io", MY_APPLICATION_ID, MY_VERSION);

void setup() {
	delay(1000);
	Serial.begin(115200);
	Serial.setDebugOutput( false);
	Serial.println();
	Serial.println("Booting  Version " + String(MY_VERSION));
	WiFi.begin(ssid, password);

	Serial.println("Starting...");
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println();
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	int result = ota.ping();

	if (result == PING_RESULT_UPDATE) {
		Serial.println("lets update to " + ota.getExtra());
		ota.update();
	} else {
		Serial.println("no update " + String(result) + " " + ota.getExtra());
	}
}

void loop() {
}
