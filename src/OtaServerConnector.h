/*
 * OtaServerConnector.h
 *
 *  Created on: 19.02.2019
 *      Author: thomas
 */

#ifndef OTASERVERCONNECTOR_H_
#define OTASERVERCONNECTOR_H_

#include "Arduino.h"
#include <ESP8266HTTPClient.h>
#include "ESP8266httpUpdate.h"
#include "ESP8266WiFi.h"

#define PING_RESULT_OK (0)
#define PING_RESULT_ERROR (-1)
#define PING_RESULT_CONNECTION_FAILED (-2)
#define PING_RESULT_UPDATE (1)
#define PING_RESULT_COMMAND (2)

class OtaServerConnector {
private:
	String baseUrl;
	String appid;
	String version;
	String extra;

	String httpRequest(String command, String payload);
public:
	OtaServerConnector(String server, String appid, String version);
	virtual ~OtaServerConnector();

	int ping();
	int update();
	String getExtra();
};

#endif /* OTASERVERCONNECTOR_H_ */
