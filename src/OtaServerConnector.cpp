/*
 * OtaServerConnector.cpp
 *
 *  Created on: 19.02.2019
 *      Author: thomas
 */

#include "OtaServerConnector.h"

OtaServerConnector::OtaServerConnector(String server, String appid,
		String version) {
	this->appid = appid;
	this->version = version;
	this->baseUrl = "http://" + server + "/otaServer/" + this->appid + "/";
}

OtaServerConnector::~OtaServerConnector() {
}

String OtaServerConnector::httpRequest(String command, String payload) {
	HTTPClient http;

	http.begin(baseUrl + command);
	http.addHeader("Content-Type", "application/x-www-form-urlencoded");
	http.addHeader("x-ESP8266-STA-MAC", WiFi.macAddress());
	http.addHeader("x-ESP8266-AP-MAC", WiFi.macAddress());
	http.addHeader("x-ESP8266-VERSION", this->version);
	http.addHeader("x-LOCAL-IP", String(WiFi.localIP()));

	int httpCode = 0;
	httpCode = http.POST(payload);

	String result;
	if (httpCode == HTTP_CODE_OK) {
		result = http.getString();
	}

	http.end();
	return result;
}

int OtaServerConnector::ping() {
	String result = httpRequest("ping", "");

	if (result.startsWith("OK")) {
		this->extra="";
		return PING_RESULT_OK;
	} else if (result.startsWith("ERROR")) {
		this->extra = result.substring(5);
		this->extra.trim();
		return PING_RESULT_ERROR;
	} else if (result.startsWith("UPDATE")) {
		this->extra = result.substring(6);
		this->extra.trim();
		return PING_RESULT_UPDATE;
	} else if (result.startsWith("CMD")) {
		this->extra = result.substring(3);
		this->extra.trim();
		return PING_RESULT_COMMAND;
	} else {
		this->extra = result;
		return PING_RESULT_CONNECTION_FAILED;
	}
}

int OtaServerConnector::update() {
	t_httpUpdate_return ret = ESPhttpUpdate.update(this->baseUrl + "update",
			this->version);
	switch (ret) {
	case HTTP_UPDATE_FAILED:
		return -1;
	case HTTP_UPDATE_NO_UPDATES:
		return 0;
	case HTTP_UPDATE_OK:
		return 1;
	default:
		return -2;
	}
}

String OtaServerConnector::getExtra() {
	return this->extra;
}
